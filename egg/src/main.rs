use tokio::io::AsyncWriteExt;
use tokio::net::TcpListener;

#[tokio::main(flavor = "current_thread")]
async fn main() {
    let server = TcpListener::bind("0.0.0.0:50413").await.unwrap();
    let (mut con, _) = server.accept().await.unwrap();
    con.write(b"Hallo, Welt!\n").await.unwrap();

    // Return error so that a job can restart this, and doesn't count it towards completion
    std::process::exit(1);
}
