#!/usr/bin/ruby

require 'json'
require 'shellwords'

$working_path = 'working'
working_path = $working_path

nodes = JSON.parse(`kubectl get nodes -o json`)['items']

def ssh(host, cmdline)
    system("sshpass -p arch ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=#{$working_path.shellescape}/known_hosts arch@#{host.shellescape} #{cmdline.shellescape}")
end

children = []

nodes = nodes.map { |node|
    node['metadata']['name']
}.select { |node|
    node.start_with?('mach')
}

nodes.each do |node|
    pid = fork
    if !pid
        ssh(node, 'sudo kubeadm reset -f')
        exit 0
    end
    children << pid
end

children.each do |child|
    Process.waitpid(child)
end

nodes.each do |node|
    system("kubectl delete node #{node.shellescape}")
end

puts
puts "Nodes:"
system("kubectl get nodes")
