Goal
====

At RH, we sometimes have days of learning, where we can learn anything
we think is worthwhile.  I’ve tried to wrap my head around containers
and Kubernetes especially for a while now, but you know, without
something to do, that’s really hard.

Also, the opaque artificial environments that you can use to try stuff
out really didn’t suit me well.

So what I want to do here is start from scratch: Create a number of VMs
on some virtual network, in a manner that I know how they’re configured
and how they work, and then have some fun with them.  Specifically, my
original intention was to deploy a distributed game of life, where one
container would simulate one pixel.

State
=====

Right now, you can spin up a cluster of VMs, one of which will hold the
control plane and a container registry, and the rest will join as nodes.
Then you can use this cluster.

Using It
========

First, you have to make a `template.qcow2`, which basically should be a
qcow2 image from https://archlinux.org/download/ (Ctrl+F “VM images”).

Then, you boot it once to let it configure itself (so the cloned images
boot faster), and you have to install a couple of packages –
`spin-up.rb 0 0` will tell you which if you just tell it that you
haven’t configured the image yet.  It will also give you a command line
to boot the image.

(If you have a concurrent `dnsmasq` instance running, you should stop
it.  If it’s part of libvirt, you should stop the respective network
with `virsh destroy #{net_name}`.)

Then you spin it up:

```
$ ./spin-up.rb 8 1024
Have you configured the template yet? [y/N] y
[...]
Verifying IPs...
daae => 10.0.0.155: Good (reported MAC: ca:fe:be:ef:da:ae)
0000 => 10.0.0.1: Good (reported MAC: ca:fe:be:ef:00:00)
4b69 => 10.0.0.109: Good (reported MAC: ca:fe:be:ef:4b:69)
2572 => 10.0.0.156: Good (reported MAC: ca:fe:be:ef:25:72)
6ce9 => 10.0.0.144: Good (reported MAC: ca:fe:be:ef:6c:e9)
70cd => 10.0.0.122: Good (reported MAC: ca:fe:be:ef:70:cd)
589a => 10.0.0.35: Good (reported MAC: ca:fe:be:ef:58:9a)
e7c0 => 10.0.0.50: Good (reported MAC: ca:fe:be:ef:e7:c0)
[...]
Nodes:
NAME             STATUS     ROLES           AGE   VERSION
control.kube     Ready      control-plane   26s   v1.26.1
mach-2572.kube   NotReady   <none>          0s    v1.26.1
mach-4b69.kube   NotReady   <none>          0s    v1.26.1
mach-589a.kube   Ready      <none>          12s   v1.26.1
mach-6ce9.kube   NotReady   <none>          0s    v1.26.1
mach-70cd.kube   NotReady   <none>          0s    v1.26.1
mach-daae.kube   NotReady   <none>          0s    v1.26.1
mach-e7c0.kube   NotReady   <none>          0s    v1.26.1
```

The four-digit hex string is a random ID, it’s also reflected in the
MAC, and the IP is handed out by a `dnsmasq` instance.  There is always
one VM with an ID of 0000, which gets the 10.0.0.1 IP – that’s the
control plane.

The control plane VM also has a DNS server, which gives each VM a
`mach-${id}.kube` host name (*mach* stands for *machine*) and itself
also the `control.kube` name.

The script also sets up *~/.kube* on your host so you can use `kubectl`
on the host to talk to the cluster.

After you’re done, you can destroy the cluster with:

```
$ ./spin-down.rb
```

The `working` directory should be empty then.

Deploying Something
===================

So taken everything together, (once you’ve configured the template)
it’ll look something like this:

```
$ ./spin-up.rb 8

$ sudo systemctl start docker
$ sudo systemctl stop containerd
$ sudo systemctl start crio
$ sudo cp cni/10-kubed.conflist /etc/cni/net.d
$ sudo kubeadm init --node-name control.kube --control-plane-endpoint=control.kube

$ mkdir -p $HOME/.kube
$ sudo cp /etc/kubernetes/admin.conf $HOME/.kube/config
$ sudo chown $(id -u):$(id -g) $HOME/.kube/config

$ ./add-nodes.rb
```

Then you wait until `kubectl get nodes` reports all of their status as
*Ready*, and then perhaps a bit more until all VMs have settled down to
less than 10 % CPU usage (otherwise pods that you try to launch may
remain in *Pending* status for a while).  Then you should be able to
use the cluster.

Afterwards:

```
$ ./remove-nodes.rb
$ sudo kubeadm reset
$ ./spin-down.rb
```


Using the cluster
=================

You can deploy containers to the registry on `control.kube` and use them
on the cluster, like so (from the host):

(nfs-egg reads `/msg` from the NFS server and serves its content on port
50413, then exits with code 1 (i.e. failure).  This allows the nfs-egg
job to auto-restart those pods, without a crash backoff timer.)

```
$ cd nfs-egg

$ cargo build
[...]

$ sudo docker image build -t control.kube:5000/nfs-egg:latest .
[...]

$ sudo docker push control.kube:5000/nfs-egg:latest
[...]
latest: digest: sha256:e6775570f364e0e3e99712edc18144998a975ca4e6085f3dee50793e57588cb4 size: 740

$ kubectl create -f nfs-egg.yml
persistentvolume/nfs-pv created
persistentvolumeclaim/nfs-pvc created
job.batch/nfs-egg-job created
service/nfs-egg-service created

$ kubectl get jobs -o wide
NAME          COMPLETIONS   DURATION   AGE   CONTAINERS   IMAGES                             SELECTOR
nfs-egg-job   0/1 of 3      4s         4s    nfs-egg      control.kube:5000/nfs-egg:latest   controller-uid=a6a611d0-0692-44ec-bfa5-55cc2c4536c8

$ kubectl get pods -o wide
NAME                READY   STATUS    RESTARTS   AGE   IP          NODE             NOMINATED NODE   READINESS GATES
nfs-egg-job-mvctk   1/1     Running   0          9s    10.1.0.4    mach-6ce9.kube   <none>           <none>
nfs-egg-job-nxw4g   1/1     Running   0          9s    10.1.0.20   mach-4b69.kube   <none>           <none>
nfs-egg-job-t7qht   1/1     Running   0          9s    10.1.0.15   mach-e7c0.kube   <none>           <none>

$ ./test-egg.sh
Hello from the PV

$ kubectl get pods -o wide
NAME                READY   STATUS              RESTARTS   AGE   IP          NODE             NOMINATED NODE   READINESS GATES
nfs-egg-job-5gzpl   0/1     ContainerCreating   0          1s    <none>      mach-6ce9.kube   <none>           <none>
nfs-egg-job-mvctk   0/1     Error               0          40s   10.1.0.4    mach-6ce9.kube   <none>           <none>
nfs-egg-job-nxw4g   1/1     Running             0          40s   10.1.0.20   mach-4b69.kube   <none>           <none>
nfs-egg-job-t7qht   1/1     Running             0          40s   10.1.0.15   mach-e7c0.kube   <none>           <none>

$ sudo mount -t nfs control.kube:/srv/nfs /mnt/tmp

$ echo 'Message changed!' > /mnt/tmp/msg

$ ./test-egg.sh
Message changed!

$ kubectl get pods -o wide
NAME                READY   STATUS    RESTARTS   AGE   IP          NODE             NOMINATED NODE   READINESS GATES
nfs-egg-job-5gzpl   1/1     Running   0          41s   10.1.0.5    mach-6ce9.kube   <none>           <none>
nfs-egg-job-m9xj4   1/1     Running   0          9s    10.1.0.21   mach-4b69.kube   <none>           <none>
nfs-egg-job-mvctk   0/1     Error     0          80s   10.1.0.4    mach-6ce9.kube   <none>           <none>
nfs-egg-job-nxw4g   0/1     Error     0          80s   10.1.0.20   mach-4b69.kube   <none>           <none>
nfs-egg-job-t7qht   1/1     Running   0          80s   10.1.0.15   mach-e7c0.kube   <none>           <none>
```

You can also run the `./clean-up-failed.sh` script in the background,
which will automatically clean up all *Error* pods every ten seconds,
and then run `./test-egg.sh` in a loop, just for fun.
