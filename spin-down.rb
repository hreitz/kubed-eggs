#!/usr/bin/ruby

require 'shellwords'


def sudo(cmd)
    puts("# #{cmd}")
    system("sudo sh -c #{cmd.shellescape}")
end


ENV['WORKING_PATH'] = 'working'
ENV['BRIDGE'] = 'qbr'

working_path = ENV['WORKING_PATH']
bridge = ENV['BRIDGE']

resolv_conf = "#{working_path}/resolv.conf"
if File.file?(resolv_conf)
    sudo("cat #{resolv_conf.shellescape} > /etc/resolv.conf")
    system("rm -f #{resolv_conf.shellescape}")
end

dnsmasq_pid = "#{working_path}/dnsmasq.pid"
if File.file?(dnsmasq_pid)
    sudo("kill #{IO.read(dnsmasq_pid).strip.shellescape}")
    system("rm -f #{dnsmasq_pid.shellescape}")
    system("rm -f #{working_path.shellescape}/dnsmasq.log")
end

sudo("ip link set #{bridge.shellescape} down")
sudo("brctl delbr #{bridge.shellescape}")

sudo("iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE")
sudo("iptables -D FORWARD -i qbr -j ACCEPT")

Dir.entries(working_path).each do |img|
    m = /^([0-9a-f]+|main)\.qcow2$/.match(img)
    next unless m

    id = m[1]
    tap = "tap-#{id}"

    pid = IO.read("#{working_path}/#{id}.pid").strip
    system("kill #{pid.shellescape}")
    sleep(0.2)

    system("rm -f #{"#{working_path}/#{id}.pid".shellescape}")

    system("rm -f #{"#{working_path}/#{img}".shellescape}")

    sudo("ip link set #{tap.shellescape} down")
    sudo("tunctl -d #{tap.shellescape}")

    system("rm -f #{"#{working_path}/#{id}.ip".shellescape}")
end

system("rm -f #{working_path.shellescape}/known_hosts")
