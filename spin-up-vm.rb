#!/usr/bin/ruby

require 'json'
require 'shellwords'


def sudo(cmd)
    puts("# #{cmd}")
    system("sudo #{cmd}")
end

def random_char
    i = (rand() * 16).to_i
    if i < 10
        (i + 48).chr
    else
        (i - 10 + 97).chr
    end
end

def random_name
    4.times.map { random_char }.join
end

def do_hyphen(val)
    if val.kind_of?(Hash)
        Hash[val.map { |k, v| [k.to_s.tr('_', '-'), do_hyphen(v)] }]
    elsif val.kind_of?(Array)
        val.map { |v| do_hyphen(v) }
    else
        val
    end
end

class Hash
    def hyphen
        do_hyphen(self)
    end

    def qmpize
        JSON.unparse(self.hyphen)
    end
end

working_path = ENV['WORKING_PATH']
template = ENV['TEMPLATE_IMG']
bridge = ENV['BRIDGE']

# Always create a 0000 machine
id = '0000'
while true
    fname = "#{working_path}/#{id}.qcow2"
    break unless File.exist?(fname)
    id = random_name
end

system("qemu-img create -f qcow2 -F qcow2 -b #{File.realpath(template).shellescape} #{fname.shellescape}")

user = `whoami`.strip
tap = "tap-#{id}"
sudo("tunctl -u #{user.shellescape} -t #{tap.shellescape}")
sudo("ip link set #{tap.shellescape} up")
sudo("brctl addif #{bridge.shellescape} #{tap.shellescape}")


child = Process.fork()
if !child
    Process.exec(
        'qemu-system-x86_64',
        '-name', id,
        '-blockdev',
        { node_name: 'hd',
          driver: 'qcow2',
          file: {
              driver: 'file',
              filename: fname,
          } }.qmpize,
        '-device', 'virtio-blk,drive=hd',
        '-enable-kvm',
        '-m', ARGV[0],
        '-device', "virtio-net,netdev=net,mac=ca:fe:be:ef:#{id[0..1]}:#{id[2..3]}",
        '-netdev', "tap,id=net,ifname=#{tap},script=no,downscript=no",
        '-display', 'none',
        '-smp', '4',
    )
    exit 1
end

IO.write("#{working_path}/#{id}.pid", "#{child}\n")
