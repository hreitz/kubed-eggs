#!/bin/sh

host=$1
shift

if [ -z "$host" ]; then
    echo "Usage: $0 <host> [cmd...]"
    echo
    echo "(<host> is expanded to mach-<host>.kube)"
    exit 1
fi

sshpass -p arch ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile="$PWD/working/known_hosts" arch@mach-"$host".kube "$@"
