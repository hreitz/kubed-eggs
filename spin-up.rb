#!/usr/bin/ruby

require 'shellwords'


def sudo(cmd)
    puts("# #{cmd}")
    system("sudo sh -c #{cmd.shellescape}")
end

def usage
    $stderr.puts("Usage: #{__FILE__} <VM count> [VM memory in MB]")
end


ENV['WORKING_PATH'] = 'working'
ENV['TEMPLATE_IMG'] = 'template.qcow2'
ENV['BRIDGE'] = 'qbr'
fallback_dns = '192.168.0.1'

if system("ps aux | grep dnsmasq | grep -v grep")
    $stderr.puts("Existing dnsmasq instances found, cannot spin up")
    $stderr.puts("(Try stopping libvirt networks)")
    exit 1
end

count = ARGV[0]
if !count
    usage()
    exit 1
end

begin
    count = Integer(count)
rescue Exception => e
    $stderr.puts(e.inspect)
    usage()
    exit 1
end

memory = ARGV[1]
if !memory
    memory = 1024
else
    begin
        memory = Integer(memory)
    rescue Exception => e
        $stderr.puts(e.inspect)
        usage()
        exit 1
    end
end

if !File.file?("certs/domain.crt")
    $stderr.puts("Please create a domain certificate for control.kube first:")
    $stderr.puts('$ openssl req -newkey rsa:4096 -nodes -sha256 -keyout certs/domain.key -addext "subjectAltName = DNS:control.kube" -x509 -days 365 -out certs/domain.crt')
    $stderr.puts
    $stderr.puts('(Don’t forget to copy this into /etc/docker/certs.d/control.kube:5000/domain.crt on the control host, too!)')
    exit 1
end

public_net = `route | grep #{'^default'.shellescape}`.strip.split[-1]

$stdout.write("Have you configured the template yet? [y/N] ")
$stdout.flush
if $stdin.readline.strip.downcase != 'y'
    puts("$ qemu-system-x86_64 -blockdev qcow2,node-name=hd,file.driver=file,file.filename=#{ENV['TEMPLATE_IMG'].shellescape} -device virtio-blk,drive=hd -enable-kvm -m #{memory}")
    puts
    puts("(Without a display, add (for ssh on 127.0.0.1:2222):)")
    puts("$ ... -display none -netdev user,id=net,hostfwd=tcp:127.0.0.1:2222-:22 -device virtio-net,netdev=net")
    puts
    puts('Please disable swap and install:')
    puts ['crictl', 'docker (and enable it)', 'cri-docker (and enable it)', 'kubeadm', 'kubectl', 'kubelet (and enable it)', 'socat', 'ethtool', 'conntrack-tools', 'bind', 'nfs-utils'].map { |pkg| " · #{pkg}\n" }
    exit
end

working_path = ENV['WORKING_PATH']
working_full_path = File.realpath(working_path)
bridge = ENV['BRIDGE']

system("mkdir -p #{working_path.shellescape}")
system("mkdir -p #{"#{working_path}/named".shellescape}")
system("chmod go+rwx #{"#{working_path}/named".shellescape}")

sudo("brctl addbr #{bridge.shellescape}")

sudo("ip address add 10.0.0.254/24 dev #{bridge.shellescape}")

sudo("echo 1 > /proc/sys/net/ipv4/ip_forward")
sudo("iptables -t nat -A POSTROUTING -o #{public_net.shellescape} -j MASQUERADE")
sudo("iptables -A FORWARD -i #{bridge.shellescape} -j ACCEPT")

sudo("dnsmasq -x #{working_full_path.shellescape}/dnsmasq.pid -i #{bridge.shellescape} --dhcp-range=10.0.0.10,10.0.0.199,255.255.255.0 --dhcp-host=10.0.0.254 --dhcp-host=ca:fe:be:ef:00:00,10.0.0.1 --dhcp-option=option:router,10.0.0.254 --dhcp-option=option:dns-server,10.0.0.1 --dhcp-ignore-clid --log-facility=#{working_full_path.shellescape}/dnsmasq.log")

#system("./spin-up-main-vm.rb")

count.times do |i|
    # Give the control plane enough memory (or kubernetes will refuse to start)
    system("./spin-up-vm.rb #{i == 0 ? "2048" : memory}")
end

sudo("ip link set #{bridge.shellescape} up")

sudo("brctl show")

while !File.file?("#{working_path}/dnsmasq.log")
    sleep(0.1)
end

puts
puts("Discovering VMs...")
sudo("chmod go+r #{working_path.shellescape}/dnsmasq.log")
log = File.open("#{working_path}/dnsmasq.log")
ip_map = {}
while ip_map.size < count
    tail = ''
    while tail.empty?
        tail = log.read
        sleep(0.1) if tail.empty?
    end

    tail.split($/).each do |line|
        m = /\bDHCPACK\(#{bridge}\)\s+([0-9.]+)\s+([0-9a-f:]+)/.match(line)
        if m
            ip_map[m[2].split(':')[-2..-1].join] = m[1]
        end
    end
end
log.close

puts
puts("Mapping handed out over DHCP:")
ip_map.each do |id, ip|
    puts("#{id}: #{ip}")
end

def ssh_line(ip, cmdline)
    "sshpass -p arch ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=#{ENV['WORKING_PATH'].shellescape}/known_hosts arch@#{ip.shellescape} #{cmdline.shellescape}"
end

def ssh(ip, cmdline)
    system(ssh_line(ip, cmdline))
end

puts
puts("Verifying IPs...")
ip_map.each do |id, ip|
    while !system("ssh-keyscan #{ip.shellescape} >> #{working_path.shellescape}/known_hosts 2>/dev/null")
        sleep(1)
    end

    mac = nil
    `#{ssh_line(ip, 'ip address show dev eth0')}`.lines.each do |line|
        line.strip!
        if line.start_with?('link/ether')
            mac = /link\/ether\s+([0-9a-f:]+)\s/.match(line)[1]
            break
        end
    end

    if !mac
        puts("\e[1m(Could not verify #{id} => #{ip})\e[0m")
    else
        expected_mac = "ca:fe:be:ef:#{id[0..1]}:#{id[2..3]}"
        if mac != expected_mac
            puts("\e[1;31m#{id} => #{ip}: MACs differ, expected #{expected_mac}, found #{mac}\e[0m")
        else
            puts("\e[1;32m#{id} => #{ip}: Good (reported MAC: #{mac})\e[0m")
        end
    end
end

ip_map.each do |id, ip|
    IO.write("#{working_path}/#{id}.ip", "#{ip}\n")
end

puts
puts("Setting up DNS on control VM...")

ssh('10.0.0.1', "echo #{<<EOF
options {
    directory "/tmp/named";
    pid-file "/tmp/named/named.pid";

    allow-recursion { any; };
    allow-transfer { none; };

    version none;
    hostname none;
    server-id none;

    forwarders {
        #{fallback_dns};
    };
};

zone "localhost" IN {
    type master;
    file "localhost.zone";
};

zone "0.0.127.in-addr.arpa" IN {
    type master;
    file "127.0.0.zone";
};

zone "kube" IN {
    type master;
    file "kube.zone";
};
EOF
.shellescape} > /tmp/named.conf")

ssh('10.0.0.1', "mkdir -p /tmp/named")
ssh('10.0.0.1', "chmod go+rwx /tmp/named")

ssh('10.0.0.1', "echo #{<<EOF
@ 1D IN SOA localhost. root.localhost. (42 3H 15M 1W 1D)
  1D IN NS localhost.
localhost. 1D IN A 127.0.0.1
localhost. 1D IN AAAA ::1
EOF
.shellescape} > /tmp/named/localhost.zone")

ssh('10.0.0.1', "echo #{<<EOF
@ 1D IN SOA   localhost. root.localhost. (42 3H 15M 1W 1D)
  1D IN NS localhost.
1.0.0.127.in-addr.arpa. 1D IN PTR localhost.
EOF
.shellescape} > /tmp/named/127.0.0.zone")

ssh('10.0.0.1', "echo #{<<EOF
$TTL 7200

@ IN SOA ns01.kube. postmaster.kube. (4 8H 30M 1W 1D)
@ IN NS ns01
ns01 IN A 10.0.0.1
control IN A 10.0.0.1
#{ip_map.map { |ipme| "mach-#{ipme[0]} IN A #{ipme[1]}" } * "\n"}
EOF
.shellescape} > /tmp/named/kube.zone")

ssh('10.0.0.1', 'sudo named -c /tmp/named.conf')

IO.write("#{working_path}/resolv.conf", IO.read('/etc/resolv.conf'))
sudo("echo 'nameserver 10.0.0.1' > /etc/resolv.conf")

puts
puts("Overriding systemd-resolved...")
ip_map.each do |ipme|
    ssh(ipme[1], 'sudo systemctl stop systemd-resolved')
    ssh(ipme[1], "sudo sh -c #{"echo 'nameserver 10.0.0.1' > /etc/resolv.conf".shellescape}")
    ssh(ipme[1], "sudo sed -i #{'s/^\(hosts:.*\) resolve\( \[[^]]*\]\)\?/\1/'.shellescape} /etc/nsswitch.conf")
end

puts("Setting VMs’ host names...")
ip_map.each do |ipme|
    ssh(ipme[1], "sudo sh -c #{"echo mach-#{ipme[0]}.kube > /etc/hostname".shellescape}")
    system("ssh-keyscan #{"mach-#{ipme[0]}.kube".shellescape} >> #{working_path.shellescape}/known_hosts 2>/dev/null")
end

puts("Copying CNI config into all VMs...")
ip_map.each do |ipme|
    system(ssh_line(ipme[1], "sudo sh -c #{"sudo cat > /etc/cni/net.d/10-kubed.conflist".shellescape}") + " < cni/10-kubed.conflist")
end

puts("Copying control.kube certificate into all VMs...")
ip_map.each do |ipme|
    ssh(ipme[1], "sudo mkdir -p /etc/docker/certs.d/control.kube:5000 /etc/ca-certificates/trust-source/anchors")
    system(ssh_line(ipme[1], "sudo sh -c #{"sudo cat > /etc/docker/certs.d/control.kube:5000/domain.crt".shellescape}") + " < certs/domain.crt")
    system(ssh_line(ipme[1], "sudo sh -c #{"sudo cat > /etc/ca-certificates/trust-source/anchors/kube.crt".shellescape}") + " < certs/domain.crt")
    ssh(ipme[1], "sudo update-ca-trust")
end

puts("Activating control node")
puts

system(ssh_line('10.0.0.1', 'cat > /tmp/kubeadm-init.conf') + " < kubeadm-init.conf")

ssh('10.0.0.1', 'sudo systemctl start docker')
ssh('10.0.0.1', 'sudo kubeadm init --config=/tmp/kubeadm-init.conf')

ssh('10.0.0.1', 'mkdir -p $HOME/.kube')
ssh('10.0.0.1', 'sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config')
ssh('10.0.0.1', 'sudo chown $(id -u):$(id -g) $HOME/.kube/config')

system('mkdir -p $HOME/.kube')
system(ssh_line('10.0.0.1', 'cat $HOME/.kube/config') + ' > $HOME/.kube/config')

puts
puts("Starting registry")
puts

ssh('10.0.0.1', 'mkdir -p $HOME/certs')
system(ssh_line('10.0.0.1', 'cat > $HOME/certs/domain.crt') + " < certs/domain.crt")
system(ssh_line('10.0.0.1', 'cat > $HOME/certs/domain.key') + " < certs/domain.key")

ssh('10.0.0.1', <<EOF
sudo docker run \
    -d \
    -p 5000:5000 \
    --restart=always \
    --name registry \
    -v "$HOME/certs:/certs" \
    -e REGISTRY_HTTP_ADDR=0.0.0.0:5000 \
    -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
    -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
    registry:2
EOF
)

puts
puts("Starting NFS server")
puts

ssh('10.0.0.1', 'sudo mkdir /srv/nfs')
ssh('10.0.0.1', "sudo sh -c #{"echo 'Hello from the PV' > /srv/nfs/msg".shellescape}")
ssh('10.0.0.1', 'sudo chmod go+rw /srv/nfs/msg')
ssh('10.0.0.1', "sudo sh -c #{"echo '/srv/nfs 10.0.0.0/24(rw,sync)' > /etc/exports".shellescape}")
ssh('10.0.0.1', 'sudo systemctl start nfs-server')
puts("(Should be on 10.0.0.1:/srv/nfs in 10.0.0.0/24.)")

puts
puts("Adding nodes")
puts

exec('./add-nodes.rb')
