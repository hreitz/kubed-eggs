#!/bin/sh

service_name='egg-service'

if kubectl get services | grep -q nfs-egg-service; then
    service_name='nfs-egg-service'
fi

kubectl port-forward service/"$service_name" 50413:50413 >/dev/null &
sleep 1
echo -ne '\033[1m'
nc localhost 50413 < /dev/null
echo -ne '\033[0m'
kill %1
sleep 1
