use tokio::fs::File;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpListener;

#[tokio::main(flavor = "current_thread")]
async fn main() {
    let server = TcpListener::bind("0.0.0.0:50413").await.unwrap();
    let (mut con, _) = server.accept().await.unwrap();

    let msg = {
        let mut file = File::open("/nfs/msg").await.unwrap();
        let mut buf = Vec::new();
        file.read_to_end(&mut buf).await.unwrap();
        buf
    };

    con.write(&msg).await.unwrap();

    // Return error so that a job can restart this, and doesn't count it towards completion
    std::process::exit(1);
}
