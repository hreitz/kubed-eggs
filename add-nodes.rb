#!/usr/bin/ruby

require 'json'
require 'shellwords'

$working_path = 'working'
working_path = $working_path

def ssh_line(host, cmdline)
    "sshpass -p arch ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=#{$working_path.shellescape}/known_hosts arch@#{host.shellescape} #{cmdline.shellescape}"
end

def control_line(cmdline)
    ssh_line('control.kube', cmdline)
end

def ssh(host, cmdline)
    system(ssh_line(host, cmdline))
end

def control(cmdline)
    system(control_line(cmdline))
end

machines = `#{control_line('cat /tmp/named/kube.zone')}`.lines.select { |l|
    l.start_with?('mach') && !l.start_with?('mach-0000')
}.map { |l|
    l.split[0] + '.kube'
}

puts('Getting token list...')
token_list = ''
60.times do |i|
    token_list = `#{control_line('kubeadm token list -o json')}`.strip
    break if !token_list.empty?
    sleep(1)
end
puts
if token_list.empty?
    puts("Failed to get token list, perhaps failed to reach control plane")
    exit 1
end

token = JSON.parse(token_list)['token']
sha = `#{control_line("openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | openssl dgst -sha256 -hex | sed 's/^.* //'")}`.strip

children = []

machines.each do |machine|
    pid = fork
    if !pid
        puts("[ssh] $ sudo kubeadm join --node-name=#{machine.shellescape} --token=#{token.shellescape} control.kube:6443 --discovery-token-ca-cert-hash=sha256:#{sha.shellescape} --ignore-preflight-errors=SystemVerification --cri-socket=unix:///var/run/cri-dockerd.sock")
        ssh(machine, "sudo kubeadm join --node-name=#{machine.shellescape} --token=#{token.shellescape} control.kube:6443 --discovery-token-ca-cert-hash=sha256:#{sha.shellescape} --ignore-preflight-errors=SystemVerification --cri-socket=unix:///var/run/cri-dockerd.sock")
        exit 0
    end
    children << pid
end

children.each do |child|
    Process.waitpid(child)
end

puts
puts "Nodes:"
system("kubectl get nodes")
